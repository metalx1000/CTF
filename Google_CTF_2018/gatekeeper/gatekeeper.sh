#!/bin/bash

wget "https://storage.googleapis.com/gctf-2018-attachments/f7e577b61f5b98aa3c0e453e83c60729f6ce3ef15c59fc76d64490377f5a0b5b" -qO gate.zip
unzip -q gate.zip
rm gate.zip

echo "Using 'Hexedit' or 'Strings' we can see this"
sleep 1

cat << EOF
===============================
Usage: %s <username> <password>                                                                                            
 ~> Verifying.                                                                                                             
 0n3_W4rM           
  ~> Incorrect username
  zLl1ks_d4m_T0g_I
  Correct!           
  Welcome back! 
  CTF{%s}              
   ~> Incorrect password
===============================
EOF

echo "Press Enter to continue..."
read

echo -e "At first I thought the flag was '0n3_W4rM'\nwhich appears to be the password"
sleep 1
echo "But it's not.  Then I saw that the username seems to be 'zLl1ks_d4m_T0g_I'"
echo "and that didn't work, but the clue of 'Nothing is the right way around' gives it away."
echo "'zLl1ks_d4m_T0g_I' backwards is 'I_g0T_m4d_sk1lLz'"

echo "Press Enter to continue..."
read

echo ""
echo "To keep it fun I'll automate pulling it from the binary file"
echo "strings gatekeeper|grep username -A 1|tail -n 1|rev"
flag="$(strings gatekeeper|grep username -A 1|tail -n 1|rev)"
echo ""
echo "CTF{$flag}"

#clean up
rm gatekeeper


echo -e "\nhttps://www.youtube.com/watch?v=xe6-6CsEbx0"
