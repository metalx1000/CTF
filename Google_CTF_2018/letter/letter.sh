#!/bin/bash

rm *.pdf
wget "https://storage.googleapis.com/gctf-2018-attachments/5a0fad5699f75dee39434cc26587411b948e0574a545ef4157e5bf4700e9d62a" -qO letter.zip
unzip -q letter.zip
pdftotext challenge.pdf -|grep CTF|awk '{print $3}'

echo -e "\nhttps://www.youtube.com/watch?v=RBjtgsGTmwI"
