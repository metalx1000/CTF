#!/bin/bash

if [ ! -f /usr/bin/fcrackzip ]
then
  echo "Please install fcrackzip"
  exit 1
fi

wget "https://storage.googleapis.com/gctf-2018-attachments/2cdc6654fb2f8158cd976d8ffac28218b15d052b5c2853232e4c1bafcb632383" -qO pass1.zip

while [ 1 ]
do
  f="$(ls -t|tail -n 1)"
  7z -aoa e -ptest $f || break
  rm $f
done

password="$(fcrackzip -v  -l 4-8 -u password.x|grep '=='|awk '{print $5}')"
echo "Using Password: $password"
7z -aoa e -p"$password" password.x

clear
echo "Flag is:"
cat password.txt
rm pass*

echo -e "\nhttps://www.youtube.com/watch?v=Av7WFOYuwz0"
