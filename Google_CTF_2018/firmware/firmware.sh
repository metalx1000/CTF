#!/bin/bash

wget "https://storage.googleapis.com/gctf-2018-attachments/9522120f36028c8ab86a37394903b100ce90b81830cee9357113c54fd3fc84bf" -O firm.zip
unzip firm.zip
rm firm.zip
gunzip challenge.ext4.gz
mkdir mnt
sudo mount challenge.ext4 mnt
sudo gunzip mnt/.mediapc_backdoor_password.gz
sudo chmod 777 mnt/.mediapc_backdoor_password
cp mnt/.mediapc_backdoor_password password
sudo umount mnt
clear 
cat password
rm challenge.ext4 mnt password -fr

echo -e "\nhttps://www.youtube.com/watch?v=csHtHUmY1-8"
